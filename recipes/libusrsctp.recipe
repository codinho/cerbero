# Copyright (c) 2014-2015, Centricular Ltd. All rights reserved.
# Copyright (c) 2015, Ericsson AB. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification,
# are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this
# list of conditions and the following disclaimer in the documentation and/or other
# materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
# INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
# OF SUCH DAMAGE.

# -*- Mode: Python -*- vi:si:et:sw=4:sts=4:ts=4:syntax=python

import shutil
from cerbero.utils import shell
from cerbero.utils import messages as m

class Recipe(recipe.Recipe):
    name = 'libusrsctp'
    version = 'master'
    remotes = { 'origin': 'https://github.com/sctplab/usrsctp.git' };
    commit = 'a572e1eaf5f7a6df08bcfb90529a8f6c20c7113d';
    stype = SourceType.GIT
    btype = BuildType.MESON
    licenses = [{License.BSD_like: ['LICENSE.md']}]

    files_libs = ['libusrsctp']
    files_devel = ['include/usrsctp.h']

    patches = [# https://github.com/sctplab/usrsctp/pull/401
               name + '/0001-meson-Dependencies-go-on-the-shared-library.patch',
               # https://github.com/sctplab/usrsctp/pull/402
               name + '/0001-meson-add-support-for-android-ios-host_system.patch',
               name + '/0001-build-include-ifaddrs.c-definition-for-android.patch',
               # https://github.com/sctplab/usrsctp/pull/403
               name + '/0001-meson-Add-support-for-building-on-Android.patch',]

    def prepare(self):
        if self.using_msvc():
            self.library_type = LibraryType.SHARED

    async def configure(self):
        usrsctplib_path = os.path.join(self.build_dir, 'usrsctplib')
        recipe_path = os.path.join(self.config.recipes_dir, self.name)
        if self.config.target_platform == Platform.ANDROID:
            self.meson_options['sctp_build_programs'] = 'false'
        elif self.config.target_platform == Platform.IOS:
            def copyreplacetree(src, dst):
                if os.path.exists(dst):
                    shutil.rmtree(dst)
                shutil.copytree(src, dst)
            copyreplacetree(os.path.join(recipe_path, "net"),
                        os.path.join(usrsctplib_path, "net"))
            self.append_env("CPPFLAGS", "-I %s" % os.path.join(usrsctplib_path, "platform"))
        await super(recipe.Recipe, self).configure()
